package com.varasofttech.entity;
import javax.persistence.*;
import java.io.Serializable;
/**
 * Created by Ranga Reddy on 1/27/2015.
 */
@Entity
@Table(name = "Employees")
public class Employee implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 249250229003886683L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String name;
    @Column
    private int age;
    @Column
    private float salary;

    public Employee() {
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public float getSalary() {
        return salary;
    }
    public void setSalary(float salary) {
        this.salary = salary;
    }
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}