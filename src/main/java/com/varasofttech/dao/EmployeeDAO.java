package com.varasofttech.dao;
import com.varasofttech.entity.Employee;
import java.util.List;
/**
 * Created by Ranga Reddy on 1/27/2015.
 */
public interface EmployeeDAO {
    public long createEmployee(Employee employee);
    public Employee updateEmployee(Employee employee);
    public void deleteEmployee(long id);
    public List<Employee> getAllEmployees();
    public Employee getEmployee(long id);
}